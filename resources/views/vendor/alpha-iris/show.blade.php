@extends('voyager-pages::layouts.default')
@section('meta_title', 'Order #' . $order->display_id)
@section('page_title', 'Order #' . $order->display_id)

@section('content')
    <div class="alpha-iris-margin-element cart-payment-page pt-10 md:pt-20">
        <h1>Order #{{ $order->display_id }}</h1>

        @if ($errors->has('payment_method'))
            @foreach ($errors->get('payment_method') as $error)
                <div class="field-error">{{ $error }}</div>
            @endforeach
        @endif


        @csrf
        <div class="flex flex-wrap overflow-hidden border-b font-bold tickets-table-headings">
            <div class="w-full overflow-hidden lg:w-3/5">Item</div>
            <div class="w-1/2 overflow-hidden lg:w-1/5">Quantity</div>
            <div class="w-1/2 overflow-hidden lg:w-1/5">Price</div>
        </div>
        @foreach ($order->items as $item)
            <div class="flex flex-wrap overflow-hidden border-b py-2 tickets-table-row">
                <div class="w-full overflow-hidden lg:w-3/5">{{ $item->name }}</div>
                <div class="w-1/2 overflow-hidden lg:w-1/5">{{ $item->quantity }}</div>
                <div class="w-1/2 overflow-hidden lg:w-1/5">{{ $item->display_total }}</div>
            </div>
        @endforeach

        @foreach ($order->totals as $total)
            <div class="flex flex-wrap overflow-hidden  py-2 tickets-table-footer-row">
                <div class="w-full overflow-hidden lg:w-3/5">&nbsp;</div>
                <div class="w-1/2 overflow-hidden lg:w-1/5 border-b font-bold cart-payment-total">{{ $total->title }}</div>
                <div class="w-1/2 overflow-hidden lg:w-1/5 border-b font-bold">{{ $total->display_total }}</div>
            </div>
        @endforeach
    </div>
@stop
