<div style="width: 100%" id="wrapper_{{$row->id}}">
<div style="width: 49%; display: inline-block">
Including Tax
<input type="number"
       class="form-control tax-aware tax-incl"
       name="{{ $row->field }}-tax-incl"
       type="number"
       onchange="calculateExclusive({{$row->id}})"
       @if($row->required == 1) required @endif
       @if(isset($options->min)) min="{{ $options->min }}" @endif
       @if(isset($options->max)) max="{{ $options->max }}" @endif
       step="{{ $options->step ?? 'any' }}"
       placeholder="{{ old($row->field, $options->placeholder ?? $row->getTranslatedAttribute('display_name')) }}"
       value="{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}">
</div>
<div style="width: 50%; display: inline-block">
Excluding Tax
<input type="number"
       class="form-control tax-excl"
       name="{{ $row->field }}"
       type="number"
       onchange="calculateInclusive({{$row->id}})"
       @if($row->required == 1) required @endif
       @if(isset($options->min)) min="{{ $options->min }}" @endif
       @if(isset($options->max)) max="{{ $options->max }}" @endif
       step="{{ $options->step ?? 'any' }}"
       placeholder="{{ old($row->field, $options->placeholder ?? $row->getTranslatedAttribute('display_name')) }}"
       value="{{ old($row->field, $dataTypeContent->{$row->field} ?? $options->default ?? '') }}">
</div>
</div>
@push('javascript')
<script>
window.TaxRate = <?php echo config('cart_manager.tax_percentage'); ?>;

function calculateExclusive(rowId){
       let wrapper = document.getElementById('wrapper_' + rowId);
       let incl = jQuery(wrapper).find('.tax-incl');
       let excl = jQuery(wrapper).find('.tax-excl');

       let tax = ( incl.val() * (window.TaxRate/(100 + window.TaxRate)) );
       excl.val(Math.round( (parseFloat(incl.val())-tax)*100)/100);
}


function calculateInclusive(rowId){
       let wrapper = document.getElementById('wrapper_' + rowId);
       let incl = jQuery(wrapper).find('.tax-incl');
       let excl = parseFloat(jQuery(wrapper).find('.tax-excl').val());
       let tax = ((excl/100) * window.TaxRate);

       incl.val( Math.round( (excl + tax) * 100)/100);
}

jQuery(document).ready(function(){
       calculateInclusive({{$row->id}});
});
</script>
@endpush