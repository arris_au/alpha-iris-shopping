<?php

use AlphaIris\Payments\Services\PaymentsService;
use AlphaIris\Shopping\Models\OrderStatus;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('key');
            $table->text('description')->nullable();
            $table->timestamps();
        });

        OrderStatus::create(['key' => 'pending', 'name' => 'Pending']);
        OrderStatus::create(['key' => 'processing', 'name' => 'Processing']);
        OrderStatus::create(['key' => 'shipped', 'name' => 'Shipped']);
        OrderStatus::create(['key' => 'complete', 'name' => 'Complete']);
        OrderStatus::create(['key' => 'cancelled', 'name' => 'Cancelled']);

        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->nullable();
            $table->foreignId('order_status_id');
            $table->integer('payment_status')->default(PaymentsService::PAYMENT_PROCESSING);
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('suburb')->nullable();
            $table->foreignId('state_id')->nullable();
            $table->string('postcode')->nullable();
            $table->foreignId('country_id')->nullable();
            $table->string('shipping_first_name');
            $table->string('shipping_last_name');
            $table->string('shipping_address_1')->nullable();
            $table->string('shipping_address_2')->nullable();
            $table->string('shipping_suburb')->nullable();
            $table->foreignId('shipping_state_id')->nullable();
            $table->string('shipping_postcode')->nullable();
            $table->foreignId('shipping_country_id')->nullable();
            $table->float('subtotal')->default(0);
            $table->float('tax')->default(0);
            $table->float('grand_total')->default(0);
            $table->timestamps();

            $table->foreign('order_status_id')->references('id')->on('order_statuses');
        });

        Schema::create('order_items', function (Blueprint $table) {
            $table->id();
            $table->foreignId('order_id');
            $table->string('name');
            $table->string('model_id');
            $table->string('model_class');
            $table->float('quantity');
            $table->float('price');
            $table->float('tax');
            $table->float('subtotal');
            $table->float('total');
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->cascadeOnDelete();
        });

        Schema::create('order_totals', function (Blueprint $table) {
            $table->id();
            $table->string('total_key');
            $table->foreignId('order_id');
            $table->integer('total_order');
            $table->string('total_class');
            $table->string('title');
            $table->float('subtotal')->default(0);
            $table->float('tax')->default(0);
            $table->float('grand_total')->default(0);
            $table->timestamps();

            $table->foreign('order_id')->references('id')->on('orders')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_totals');
        Schema::dropIfExists('order_items');
        Schema::dropIfExists('orders');
        Schema::dropIfExists('order_statuses');
    }
}
