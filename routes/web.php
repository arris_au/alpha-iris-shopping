<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('shopping/orders/{order}', '\AlphaIris\Shopping\Http\Controllers\CheckoutController@show')->name('shopping.order.show');
});

Route::group(['middleware' => ['web'], 'as' => 'voyager.'], function () {
    Route::get('admin/orders/{id}/clear_payment', [
        'uses' => '\AlphaIris\Shopping\Http\Controllers\ClearPaymentController@clear_payment',
        'as' => 'orders.clear_payment',
    ]);
});