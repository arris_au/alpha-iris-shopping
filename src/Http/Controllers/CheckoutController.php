<?php

namespace AlphaIris\Shopping\Http\Controllers;

use AlphaIris\Payments\Services\Payments;
use AlphaIris\Payments\Services\PaymentsService;
use AlphaIris\Shopping\Events\OrderPaidEvent;
use AlphaIris\Shopping\Models\Order;
use AlphaIris\Shopping\Models\OrderStatus;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;

class CheckoutController extends Controller
{
    public function index()
    {
    }

    public function show(Order $order)
    {
        return view('alpha-iris-shopping::show', ['order' => $order]);
    }

    public function pay(Request $request, Order $order)
    {
        $methods = Payments::availableMethods();

        return view('alpha-iris-shopping::payment', ['request'=> $request, 'order' => $order, 'methods' => $methods]);
    }

    public function store(Request $request, Order $order)
    {
    }

    public function update(Request $request, Order $order)
    {
        $methods = Payments::availableMethods();
        $request->validate(['payment_method' => 'required']);
        $name = $request->get('payment_method');
        $method = $methods->filter(function ($model) use ($name) {
            return $model->getKey() == $name;
        })->first();

        $method->validate($request);
        $payment_status = $method->process($request, $order->grand_total, $order->id);

        if (is_object($payment_status)) {
            return $payment_status;
        } else {
            $order->payment_status = $payment_status;
            $paidStatus = null;
            if ($payment_status == PaymentsService::PAYMENT_CLEARED) {
                $paidStatus = setting('shopping.order_paid_status');
                if ($paidStatus) {
                    $order->order_status_id = $paidStatus;
                }
            }
            $order->save();
            if ($paidStatus) {
                OrderPaidEvent::dispatch($order);
            }
        }

        return response()->redirectTo(route('shopping.order.show', $order));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $rules = [];
        $allFields = $allFields = $this->getPublicFields();
        foreach ($allFields as $field) {
            if ($field->required) {
                $rules[$field->field] = ['required'];
            }
        }

        return Validator::make($data, $rules);
    }
}
