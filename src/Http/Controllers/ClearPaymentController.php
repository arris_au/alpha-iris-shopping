<?php

namespace AlphaIris\Shopping\Http\Controllers;

use AlphaIris\Core\Models\User;
use AlphaIris\Core\Models\UserMembership;
use AlphaIris\Payments\Services\PaymentsService;
use AlphaIris\Shopping\Models\Order;
use Illuminate\Routing\Controller;
use AlphaIris\Events\Models\TicketSale;

class ClearPaymentController extends Controller
{
    public function clear_payment($id)
    {
        $order = Order::find($id);
        
        if ($order && $order->items()->where('model_class', UserMembership::class)->exists()) {
            if ($order->user_id) {
                $user = User::find($order->user_id);
                $membership = $user->membership;

                $membership->payment_status = PaymentsService::PAYMENT_CLEARED;
                $membership->status = UserMembership::MEMBERSHIP_ACTIVE;
                $membership->paid_at = now();
                $membership->save();
            }
        }

        if ($order && $order->items()->where('model_class', TicketSale::class)->exists()) {
            $item_ids = $order->items->where('model_class', TicketSale::class)->pluck('model_id');

            TicketSale::whereIn('id', $item_ids)->update([
                'order_payment_status' => PaymentsService::PAYMENT_CLEARED,
            ]);
        }

        $order->payment_status = PaymentsService::PAYMENT_CLEARED;

        $order->save();

        return back();
    }
}
