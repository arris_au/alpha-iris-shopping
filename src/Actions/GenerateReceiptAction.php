<?php

namespace AlphaIris\Shopping\Actions;

use TCG\Voyager\Actions\AbstractAction;
use AlphaIris\Payments\Services\PaymentsService;

class GenerateReceiptAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Generate Receipt';
    }

    public function getIcon()
    {
        return 'voyager-download';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary float-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('user.orders.receipt_download', $this->data->{$this->data->getKeyName()});
    }

    public function shouldActionDisplayOnDataType()
    {
        return in_array($this->dataType->slug, [
            'orders',
        ]);
    }

    public function shouldActionDisplayOnRow($row)
    {
        return $row->payment_status == PaymentsService::PAYMENT_CLEARED;
    }
}