<?php

namespace AlphaIris\Shopping\Actions;

use TCG\Voyager\Actions\AbstractAction;
use AlphaIris\Payments\Services\PaymentsService;

class EmailReceiptAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Email Receipt';
    }

    public function getIcon()
    {
        return 'voyager-mail';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary float-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('user.orders.email_receipt', $this->data->{$this->data->getKeyName()});
    }

    public function shouldActionDisplayOnDataType()
    {
        return in_array($this->dataType->slug, [
            'orders',
        ]);
    }

    public function shouldActionDisplayOnRow($row)
    {
        return $row->payment_status == PaymentsService::PAYMENT_CLEARED && ! $row->user_id;
    }
}