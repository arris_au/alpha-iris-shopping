<?php

namespace AlphaIris\Shopping\Actions;

use TCG\Voyager\Actions\AbstractAction;
use AlphaIris\Payments\Services\PaymentsService;

class ClearPaymentAction extends AbstractAction
{
    public function getTitle()
    {
        return 'Clear Payment';
    }

    public function getIcon()
    {
        return 'voyager-check';
    }

    public function getPolicy()
    {
        return 'edit';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-primary float-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('voyager.orders.clear_payment', $this->data->{$this->data->getKeyName()});
    }

    public function shouldActionDisplayOnDataType()
    {
        return in_array($this->dataType->slug, [
            'orders',
        ]);
    }

    public function shouldActionDisplayOnRow($row)
    {
        return $row->payment_status != PaymentsService::PAYMENT_CLEARED;
    }
}