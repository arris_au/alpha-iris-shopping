<?php

namespace AlphaIris\Shopping\Totals;

use AlphaIris\Shopping\Models\Order;

class SubTotal extends AbstractTotal
{
    public static function calculate(Order $order)
    {
        $lineSubtotal = $order->items()->sum('subtotal');

        return [
            'subtotal' => $lineSubtotal,
            'tax' => $lineSubtotal,
            'grand_total' => $lineSubtotal,
        ];
    }
}
