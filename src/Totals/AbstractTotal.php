<?php

namespace AlphaIris\Shopping\Totals;

use AlphaIris\Shopping\Models\Order;

abstract class AbstractTotal
{
    abstract public static function calculate(Order $order);

    public static function isCumulativeToTotal()
    {
        return false;
    }
}
