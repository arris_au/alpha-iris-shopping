<?php

namespace AlphaIris\Shopping\Totals;

use AlphaIris\Shopping\Models\Order;

class GrandTotal extends AbstractTotal
{
    public static function calculate(Order $order)
    {
        $lineSubtotal = $order->items()->sum('subtotal');
        $lineTax = $order->items()->sum('tax');
        $lineTotal = $order->items()->sum('total');

        foreach ($order->totals as $total) {
            $class = $total->total_class;

            if ($class::isCumulativeToTotal()) {
                $lineSubtotal += $total->price;
                $lineTax += $total->tax;
                $lineTotal += $total->total;
            }
        }

        return [
            'subtotal' => $lineSubtotal,
            'tax' => $lineTax,
            'grand_total' => $lineTotal,
        ];
    }
}
