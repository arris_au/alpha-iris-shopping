<?php

namespace AlphaIris\Shopping\Totals;

use AlphaIris\Shopping\Models\Order;

class TaxTotal extends AbstractTotal
{
    public static function calculate(Order $order)
    {
        $lineTax = $order->items()->sum('tax');

        return [
            'subtotal' => $lineTax,
            'tax' => $lineTax,
            'grand_total' => $lineTax,
        ];
    }
}
