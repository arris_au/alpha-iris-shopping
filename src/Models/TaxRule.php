<?php

namespace AlphaIris\Shopping\Models;

use AlphaIris\Core\Models\Country;
use AlphaIris\Core\Models\State;
use AlphaIris\Payments\Services\Payments;
use Illuminate\Database\Eloquent\Model;

class TaxRule extends Model
{
    protected $fillable = [
        'name',
        'code',
        'tax_rate',
        'buyer_countries',
        'buyer_states',
        'enabled',
    ];

    public function setBuyerCountriesAttribute($value)
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }
        $this->attributes['buyer_countries'] = $value;
    }

    public function setBuyerStatesAttribute($value)
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }
        $this->attributes['buyer_states'] = $value;
    }

    public function getBuyerCountriesAttribute($value)
    {
        $value = json_decode($value);

        return is_array($value) ? $value : [];
    }

    public function getBuyerStatesAttribute($value)
    {
        $value = json_decode($value);

        return is_array($value) ? $value : [];
    }

    public function appliesToCountry($country)
    {
        if ($country instanceof Country) {
            $country = $country->id;
        }
        if (count($this->buyer_countries) > 0) {
            return in_array($country, $this->buyer_countries);
        } else {
            return true;
        }
    }

    public function applicable($country, $state)
    {
        return $this->appliesToCountry($country) && $this->appliesToState($state);
    }

    public function calculate($amountEx)
    {
        return $amountEx * ($this->tax_rate);
    }

    public function appliesToState($state)
    {
        if ($state instanceof State) {
            $stateModel = $state;
            $state = $state->id;
        } else {
            $stateModel = State::find($state);
        }

        if (count($this->buyer_states) > 0) {
            $specificState = in_array($state, $this->buyer_states);
            if ($specificState) {
                return true;
            } else {
                return $this->appliesToCountry($stateModel->country_id) && $this->states->where('country_id', $stateModel->country_id) == 0;
            }
        } else {
            return true;
        }
    }

    public function countries()
    {
        return $this->hasMany(Country::class, 'id', 'buyer_countries');
    }

    public function states()
    {
        return $this->hasMany(State::class, 'id', 'buyer_states');
    }
}
