<?php

namespace AlphaIris\Shopping\Models;

use AlphaIris\Core\Models\Country;
use AlphaIris\Core\Models\State;
use AlphaIris\Core\Services\AlphaIris;
use AlphaIris\Shopping\Totals\GrandTotal;
use AlphaIris\Shopping\Totals\SubTotal;
use AlphaIris\Shopping\Totals\TaxTotal;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use PDF;

class Order extends Model
{
    protected $fillable = [
        'user_id',
        'order_status_id',
        'payment_status',
        'company',
        'first_name',
        'last_name',
        'email',
        'phone',
        'address_1',
        'address_2',
        'suburb',
        'state_id',
        'postcode',
        'country_id',
        'shipping_first_name',
        'shipping_last_name',
        'shipping_address_1',
        'shipping_address_2',
        'shipping_suburb',
        'shipping_state_id',
        'shipping_postcode',
        'shipping_country_id',
        'subtotal',
        'tax',
        'grand_total',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function totals()
    {
        return $this->hasMany(OrderTotal::class)->orderBy('total_order');
    }

    public function getDisplayIdAttribute()
    {
        return Str::padLeft($this->id, 8, '0');
    }

    public function getHtmlFormattedAddressAttribute()
    {
        $address = AlphaIris::formatAddress(
            $this->address_1,
            $this->address_2,
            $this->suburb,
            $this->postcode,
            $this->state,
            $this->country
        );

        return str_replace("\n", '<br>', $address);
    }

    public function addItem(Model $model, $name, $itemPrice, $qty = 1)
    {
        if (! $this->id) {
            throw new \Exception('Order must be saved before adding items');
        }

        $itemTax = round(($itemPrice * config('cart_manager.tax_percentage')) / 100, 2);
        /**
        foreach (TaxRule::all() as $taxRule) {
            if ($taxRule->applicable($this->country_id, $this->state_id)) {
                $itemTax += $taxRule->calculate($itemPrice);
            }
        }*/
        $item = OrderItem::create([
            'order_id' => $this->id,
            'name' => $name,
            'model_id' => $model->id,
            'model_class' => get_class($model),
            'quantity' => $qty,
            'price' => $itemPrice,
            'tax' => $itemTax,
            'total' => ($itemPrice + $itemTax) * $qty,
            'subtotal' => $itemPrice * $qty,
        ]);

        $this->calculateTotals();

        return $item;
    }

    public function calculateTotals()
    {
        $grandTotal = $this->ensureTotal('grand-total', GrandTotal::class, 'Grand Total', 999);
        $subTotal = $this->ensureTotal('subtotal', SubTotal::class, 'Sub-Total', 899);
        $taxTotal = $this->ensureTotal('tax-total', TaxTotal::class, 'Tax', 799);

        $this->subtotal = $grandTotal->subtotal;
        $this->tax = $grandTotal->tax;
        $this->grand_total = $grandTotal->grand_total;
        $this->save();
    }

    protected function ensureTotal($key, $class, $title, $order)
    {
        $total = OrderTotal::where('total_key', $key)->where('order_id', $this->id)->first();
        if (! $total) {
            $total = OrderTotal::create([
                'total_key' => $key,
                'order_id' => $this->id,
                'total_class' => $class,
                'title' => $title,
                'total_order' => $order,
            ]);
        }
        $total->calculate();

        return $total;
    }

    public static function receiptPDF(Order $order)
    {
        $disk = Storage::disk('local');
        if (! $disk->exists('receipts')) {
            $disk->makeDirectory('receipts');
        }

        $receipt = $disk->path('receipts/Receipt_'.$order->id.'.pdf');
        if (! $disk->exists('receipts/Receipt'.$order->id.'.pdf')) {
            $pdf = PDF::loadView('pdfs.order_receipt', ['order' => $order]);
            $pdf->save($receipt);
        }

        return $receipt;
    }
}
