<?php

namespace AlphaIris\Shopping\Models;

use AlphaIris\Payments\Services\Payments;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id', 'name', 'model_id', 'model_class', 'quantity', 'price', 'tax', 'subtotal', 'total',
    ];

    public function getDisplayTotalAttribute()
    {
        return Payments::formatCurrency($this->total);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
