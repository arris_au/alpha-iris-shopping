<?php

namespace AlphaIris\Shopping\Models;

use AlphaIris\Payments\Services\Payments;
use Illuminate\Database\Eloquent\Model;

class OrderTotal extends Model
{
    protected $fillable = [
        'order_id',
        'total_key',
        'total_order',
        'total_class',
        'title',
        'subtotal',
        'tax',
        'grand_total',
    ];

    public function getDisplayTotalAttribute()
    {
        return Payments::formatCurrency($this->grand_total);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function calculate()
    {
        $class = $this->total_class;
        $totals = $class::calculate($this->order);
        $this->subtotal = $totals['subtotal'];
        $this->tax = $totals['tax'];
        $this->grand_total = $totals['grand_total'];
        $this->save();
    }
}
