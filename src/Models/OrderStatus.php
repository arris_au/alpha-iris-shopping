<?php

namespace AlphaIris\Shopping\Models;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    protected $fillable = [
        'name', 'description', 'key',
    ];
}
