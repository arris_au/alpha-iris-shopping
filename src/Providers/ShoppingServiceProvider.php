<?php

namespace AlphaIris\Shopping\Providers;

use AlphaIris\Shopping\Core\AICart;
use AlphaIris\Shopping\FormFields\TaxAwareFormField;
use AlphaIris\Shopping\Models\OrderStatus;
use Freshbitsweb\LaravelCartManager\Contracts\CartDriver;
use Freshbitsweb\LaravelCartManager\Core\Cart;
use Illuminate\Foundation\Support\Providers\EventServiceProvider;
use Illuminate\Support\Facades\App;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Models\Setting;

class ShoppingServiceProvider extends EventServiceProvider
{
    public function boot()
    {
        //$router = $this->app->make(Router::class);
        //$router->pushMiddlewareToGroup('web', RegistrationPayment::class);
        $this->loadMigrationsFrom([
            __DIR__.'/../../database/migrations',
            app_path('/../vendor/freshbitsweb/laravel-cart-manager/database/migrations/'),
        ]);
        $this->loadRoutesFrom(__DIR__.'../../../routes/web.php');

        Voyager::addAction(\AlphaIris\Shopping\Actions\GenerateReceiptAction::class);
        Voyager::addAction(\AlphaIris\Shopping\Actions\EmailReceiptAction::class);
        Voyager::addAction(\AlphaIris\Shopping\Actions\ClearPaymentAction::class);

        try {
            $pending = OrderStatus::where('key', 'pending')->first()->id;
            $processing = OrderStatus::where('key', 'processing')->first()->id;
        } catch (\Exception $e) {
            $pending = null;
            $processing = null;
        }
        $setting = Setting::firstOrNew(['key' => 'shopping.default_order_status']);
        $setting->fill([
            'display_name' => 'Default Order Status',
            'type' => 'table_select',
            'order' => 7,
            'details' => json_encode(
                [
                    'default' => $pending,
                    'model' => OrderStatus::class,
                    'index_name' => 'id',
                    'display_name' => 'name',
                ]
            ),
            'group' => 'Shopping',
        ])->save();

        Voyager::addFormField(TaxAwareFormField::class);

        $setting = Setting::firstOrNew(['key' => 'shopping.order_paid_status']);
        $setting->fill([
            'display_name' => 'Order Paid Status',
            'type' => 'table_select',
            'order' => 7,
            'details' => json_encode(
                [
                    'default' => $processing,
                    'model' => OrderStatus::class,
                    'index_name' => 'id',
                    'display_name' => 'name',
                ]
            ),
            'group' => 'Shopping',
        ])->save();

        // Bind the cart class
        $this->app->bind(Cart::class, function ($app) {
            return new AICart($app->make(CartDriver::class));
        });
    }

    public function register()
    {
        $this->loadViewsFrom([
            App::basePath().'/resources/views/alpha-iris-shopping',
            __DIR__.'/../../resources/views/',
            __DIR__.'/../../resources/views/vendor/alpha-iris',
        ], 'alpha-iris-shopping');

        $this->loadViewsFrom([
            __DIR__.'/../../resources/views/vendor/voyager',
        ], 'voyager');

        // Bind the cart class
        $this->app->bind(AICart::class, function ($app) {
            return new AICart($app->make(CartDriver::class));
        });
    }
}
