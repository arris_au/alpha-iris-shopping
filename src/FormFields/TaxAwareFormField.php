<?php

namespace AlphaIris\Shopping\FormFields;

use TCG\Voyager\FormFields\AbstractHandler;

class TaxAwareFormField extends AbstractHandler
{
    protected $codename = 'taxaware';

    public function createContent($row, $dataType, $dataTypeContent, $options)
    {
        return view('alpha-iris-shopping::formfields.taxaware', [
            'row' => $row,
            'options' => $options,
            'dataType' => $dataType,
            'dataTypeContent' => $dataTypeContent,
        ]);
    }
}
