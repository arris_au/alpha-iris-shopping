<?php

namespace AlphaIris\Shopping\Core;

use Freshbitsweb\LaravelCartManager\Core\CartItem;

class AICartItem extends CartItem
{
    public $tax;
    public $price_inc;

    public function __construct($data, $quantity)
    {
        parent::__construct($data, $quantity);

        $this->tax = round(($this->price * config('cart_manager.tax_percentage')) / 100, 2);
        $this->price_inc = $this->price + $this->tax;
    }

    /**
     * Creates a new cart item from an array or entity.
     *
     * @param Illuminate\Database\Eloquent\Model|array
     * @param int Quantity of the item
     * @return \Freshbitsweb\LaravelCartManager\Core\CartItem
     */
    public static function createFrom($data, $quantity = 1)
    {
        return new static($data, $quantity);
    }


    public function toArray()
    {
        $data = parent::toArray();
        $data['tax'] = $this->tax;
        $data['price_inc'] = $this->price_inc;

        return $data;
    }
}
