<?php

namespace AlphaIris\Shopping\Core;

use Freshbitsweb\LaravelCartManager\Core\Cart;
use Freshbitsweb\LaravelCartManager\Events\CartItemAdded;

class AICart extends Cart
{
    /**
     * Adds an item to the cart.
     *
     * @param Illuminate\Database\Eloquent\Model
     * @param int Quantity
     * @return array
     */
    public function add($entity, $quantity)
    {
        if ($this->itemExists($entity)) {
            $cartItemIndex = $this->items->search($this->cartItemsCheck($entity));

            return $this->incrementQuantityAt($cartItemIndex, $quantity);
        }

        $this->items->push(AICartItem::createFrom($entity, $quantity));

        event(new CartItemAdded($entity));

        return $this->cartUpdates($isNewItem = true);
    }

    /**
     * Creates CartItem objects from the data.
     *
     * @param array Cart items data
     * @return void
     */
    protected function setItems($cartItems)
    {
        foreach ($cartItems as $cartItem) {
            $this->items->push(AICartItem::createFrom($cartItem));
        }
    }
}
